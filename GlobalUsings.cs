﻿global using HarmonyLib;

global using System;
global using System.Diagnostics;
global using System.Reflection;
global using System.Reflection.Metadata.Ecma335;
global using System.Runtime.ExceptionServices;
global using System.Text;
global using System.Text.Encodings.Web;
global using System.Text.Json;
global using System.Text.Json.Serialization;
