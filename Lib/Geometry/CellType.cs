﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ACE.Mods.PathFinding.Lib.Geometry
{
    public enum CellType
    {
        Terrain,
        Indoors,
        Dungeon
    }
}
